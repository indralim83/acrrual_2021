<?php
class M_jurnal extends CI_Model{

	function get_nojurnal($initial){
		
		$th = date('y');
		$initial=substr($initial,0,2);
		$initial=strtoupper($initial);
        
		$q = $this->db->query("SELECT MAX(RIGHT(no_urut,5))  as noakhir FROM gl_m_voucher where left(no_urut,2)='$initial'");
        

		if($q->num_rows()>0){
			  foreach($q->result() as $k){
            	$tmp = ((int)$k->noakhir)+1;
		    	$kode1	= $initial.$th.sprintf("%05s",$tmp);
		        $koderec=$kode1;
            	}
        }else{
            	$kode1	= $initial.$th.'10001';
		        $koderec=$kode1;
        }
		    
		return $koderec;
		
	}

	function simpan_header($jurnal_kode,$tgl_issue,$tgl_book,$novoucher,$jenis,$dibayar){
		$idadmin=$this->session->userdata('idadmin');
		$this->db->query("INSERT INTO gl_m_voucher (no_urut,issue_date,booking_date,no_voucher,jenis_voucher,dibayar,status) VALUES ('$jurnal_kode','$tgl_issue','$tgl_book','$novoucher','$jenis','$dibayar',0)");
		
		//$this->db->query("INSERT INTO gl_m_voucher (no_urut,issue_date,booking_date,no_voucher,dibayar,status,jenis_voucher) VALUES ('$jurnal_kode','$tgl_issue','$tgl_book','$novoucher','$dibayar','0',$jenis')");
		/*
		foreach ($this->cart->contents() as $item) {
			$data=array(
				'd_beli_nofak' 		=>	$nofak,
				'd_beli_barang_id'	=>	$item['id'],
				'd_beli_harga'		=>	$item['price'],
				'd_beli_jumlah'		=>	$item['qty'],
				'd_beli_total'		=>	$item['subtotal'],
				'd_beli_kode'		=>	$beli_kode
			);
			$this->db->insert('tbl_detail_beli',$data);
		
		}
		*/	
		return true;	


	}

	function simpan_detail($nourut,$kode_coa,$kode_bagian,$keterangan,$kas_bank,$rel,$cust,$debet,$kredit,$username){
		$idadmin=$this->session->userdata('idadmin');
		$this->db->query("INSERT INTO gl_d_voucher (no_urut,kode_coa,kode_bagian,keterangan,kas_bank,rel,cust,debet,kredit,username) VALUES ('$nourut','$kode_coa','$kode_bagian','$keterangan','$kas_bank','$rel','$cust','$debet','$kredit','$username')");
		
		return true;	


	}

	function simpan_detail1($nourut){
		$idadmin=$this->session->userdata('idadmin');
		$this->db->query("INSERT INTO gl_d_voucher (no_urut) VALUES ('$nourut')");
		
		return true;	


	}
}
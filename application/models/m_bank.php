<?php
class m_bank extends CI_Model{

	function hapus_suplier($kode){
		$hsl=$this->db->query("DELETE FROM tb_kode_perkiraan where coa_property='$kode'");
		return $hsl;
	}

	function update_suplier($kode,$nama,$alamat,$notelp){
		$hsl=$this->db->query("UPDATE tb_kode_perkiraan set suplier_nama='$nama',suplier_alamat='$alamat',suplier_notelp='$notelp' where suplier_id='$kode'");
		return $hsl;
	}

	function tampil_(){
		$hsl=$this->db->query("select * from gl_bank order by code desc");
		return $hsl;
	}

	function simpan_bank($code,$name,$bank1,$coa_bank_property,$account_no,$address,$phone,$fax,$contact,$gl_account,$ap_account,$aktif){
		$hsl=$this->db->query("INSERT INTO gl_bank(code, bank1, coa_bank_property, name, account_no, address, phone, fax, contact, gl_account, ap_account, aktif) 
			VALUES ('$code','$name','$bank1','$coa_bank_property','$account_no','$address','$phone','$fax','$contact','$gl_account','$ap_account','$aktif')");
		return $hsl;
	}

	function update_bank($code,$name,$bank1,$coa_bank_property,$account_no,$address,$phone,$fax,$contact,$gl_account,$ap_account,$aktif){
		$hsl=$this->db->query("
			UPDATE db_charitas.gl_bank
				SET bank1='$bank1', 
				coa_bank_property='$coa_bank_property', 
				name='$name', 
				account_no='$account_no', 
				address='$address', 
				phone='$phone', 
				fax='$fax', 
				contact='$contact', 
				gl_account='$gl_account', 
				ap_account='$ap_account', 
				aktif='$aktif'
			WHERE code='$code'
		");
		return $hsl;
	}

}

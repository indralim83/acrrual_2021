<?php
class m_coa extends CI_Model{

	function hapus_suplier($kode){
		$hsl=$this->db->query("DELETE FROM tb_kode_perkiraan where coa_property='$kode'");
		return $hsl;
	}

	function update_suplier($kode,$nama,$alamat,$notelp){
		$hsl=$this->db->query("UPDATE tb_kode_perkiraan set suplier_nama='$nama',suplier_alamat='$alamat',suplier_notelp='$notelp' where suplier_id='$kode'");
		return $hsl;
	}

	function tampil_coa(){
		$hsl=$this->db->query("select * from tb_kode_perkiraan where aktif='Y' order by coa_property desc");
		return $hsl;
	}
	function get_coa($kobar){
		$hsl=$this->db->query("SELECT * FROM tb_kode_perkiraan where kode_perkiraan='$kobar' and aktif='Y'");
		return $hsl;
	}
	function simpan_coa($kode_perkiraan, $coa_property, $nama_perkiraan, $kelompok, $nama_kelompok, $hheader, $jenis, $rel, $cust, $cash_bank, $neraca, $aktif){
		$hsl=$this->db->query(
			"INSERT INTO db_charitas.tb_kode_perkiraan
			(kode_perkiraan, coa_property, nama_perkiraan, kelompok, nama_kelompok, hheader, jenis, rel, cust, cash_bank, neraca, aktif )
			VALUES('$kode_perkiraan', '$coa_property', '$nama_perkiraan', '$kelompok', '$nama_kelompok', '$hheader', '$jenis', '$rel', '$cust', '$cash_bank', '$neraca', '$aktif');");
		return $hsl;
	}

	function getAllCoa() {
		return $this->db->query("Select * from tb_kode_perkiraan order by kode_perkiraan ");
	}

	function update_coa($kode_perkiraan, $coa_property, $nama_perkiraan, $kelompok, $nama_kelompok, $hheader, $jenis, $rel, $cust, $cash_bank, $neraca, $aktif){
		$hsl=$this->db->query(
			"UPDATE db_charitas.tb_kode_perkiraan 
			SET 
				coa_property ='$coa_property', 
				nama_perkiraan = '$nama_perkiraan', 
				kelompok='$kelompok', 
				nama_kelompok='$nama_kelompok', 
				hheader='$hheader', 
				jenis='$jenis', 
				rel='$rel', 
				cust='$cust', 
				cash_bank='$cash_bank', 
				neraca='$neraca', 
				aktif='$aktif'
			WHERE kode_perkiraan = '$kode_perkiraan'
			");
		return $hsl;
	}

}
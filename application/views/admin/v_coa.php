<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Produk By Mfikri.com">
    <meta name="author" content="M Fikri Setiadi">

    <title>Welcome To Point of Sale Apps</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url().'assets/css/bootstrap.min.css'?>" rel="stylesheet">
	<link href="<?php echo base_url().'assets/css/style.css'?>" rel="stylesheet">
	<link href="<?php echo base_url().'assets/css/font-awesome.css'?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url().'assets/css/4-col-portfolio.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/dataTables.bootstrap.min.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/jquery.dataTables.min.css'?>" rel="stylesheet">

</head>

<body>

    <!-- Navigation -->
   <?php 
        $this->load->view('admin/menu');
   ?>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Daftar COA
                    <div class="pull-right"><a href="#" class="btn btn-sm btn-success" data-toggle="modal" data-target="#largeModal"><span class="fa fa-plus"></span> Tambah </a></div>
                </h1>
            </div>
        </div>
        <!-- /.row -->
        <!-- Projects Row -->
        <div class="row">
            <div class="col-lg-12">
            <table class="table table-bordered table-condensed" style="font-size:11px;" id="mydata">
                <thead>
                    <tr>
                        <th>No</th>
                        <th style="text-align:center;width:40px;">kode</th>
                        <th>Nama</th>
                        <th>kelompok</th>
                        <th>Nama Kel.</th>
                        <th>Header</th>
                        <th>D/K</th>
                        <th>Neraca/LR</th>
                        <th>Bank</th>
                        <th>Rel</th>
                        <th>Aktif</th>
                        <th style="width:140px;text-align:center;">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    $no=0;
                    foreach ($data->result_array() as $coa):
                        $no++;
                        $kode=$coa['kode_perkiraan'];
                        $nama=$coa['nama_perkiraan'];
                        $kelompok=$coa['kelompok'];
                        $namaKelompok=$coa['nama_kelompok'];
                        $hheader=$coa['hheader'];
                        $jenis=$coa['jenis'];
                        $neraca=$coa['neraca'];
                        $cashBank=$coa['cash_bank'];
                        $cust=$coa['cust'];
                        $rel=$coa['rel'];
                        $aktif=$coa['aktif'];
                ?>
                    <tr>
                        <td><?php echo $no;?></td>
                        <td style="text-align:center;"><?php echo $kode;?></td>
                        <td><?php echo $nama;?></td>
                        <td><?php echo $kelompok;?></td>
                        <td><?php echo $namaKelompok;?></td>
                        <td><?php echo $hheader;?></td>
                        <td><?php echo $jenis;?></td>
                        <td><?php echo $neraca;?></td>
                        <td><?php echo $cashBank;?></td>
                        <td><?php echo $rel;?></td>
                        <td><?php echo $aktif;
                            // if ($aktif ==1) {
                            //     echo "Y";
                            // } else {
                            //     echo "N";
                            // }
                            ?>
                        </td>
                        <td style="text-align:center;">
                            <a class="btn btn-xs btn-warning" href="#modalEditCoa<?php echo $kode?>" data-toggle="modal" title="Edit"><span class="fa fa-edit"></span> Edit</a>
                            <!-- <a class="btn btn-xs btn-danger" href="#modalHapusBank<?php echo $kode?>" data-toggle="modal" title="Hapus"><span class="fa fa-close"></span> Hapus</a> -->
                        </td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
            </div>
        </div>
        <!-- /.row -->
        <!-- ============ MODAL ADD =============== -->
        <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title" id="myModalLabel">Tambah </h3>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url().'admin/Coa/addnew'?>">
                <div class="modal-body">

                        <div class="form-group">
                            <label class="control-label col-xs-3" >kode</label>
                            <div class="col-xs-9">
                                <input name="kode" class="form-control" type="text"  style="width:280px;" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-xs-3" >Nama</label>
                            <div class="col-xs-9">
                                <input name="nama" class="form-control" type="text"  style="width:280px;" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-3" >kelompok</label>
                            <div class="col-xs-9">
                                <input name="kelompok" class="form-control" type="text" style="width:280px;" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-3" >namaKelompok</label>
                            <div class="col-xs-9">
                                <input name="namaKelompok" class="form-control" type="text" style="width:280px;" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-3" >coa_property</label>
                            <div class="col-xs-9">
                                <input name="coa_property" class="form-control" type="text" style="width:280px;" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-3" >hheader</label>
                            <div class="col-xs-9">
                                <input name="hheader" class="form-control" type="text" style="width:280px;" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-3" >Jenis </label>
                            <div class="col-xs-9">
                                <select  name="jenis" class="selectpicker show-tick form-control"  title="Pilih Status" style="width:60%;max-width:50%;" placeholder="Pilih " required>
                                    <option value ='D' >Debet</option>
                                    <option value ='K' >Kredit</option>
                                </select>
                            </div>
                        </div> 

                        <div class="form-group">
                            <label class="control-label col-xs-3" >cust </label>
                            <div class="col-xs-9">
                                <!-- <input name="cust" class="form-control" type="text" value="<?php echo $cust;?>" style="width:280px;" required> -->
                                <select  name="cust" class="selectpicker show-tick form-control"  title="Pilih " style="width:60%;max-width:50%;" placeholder="Pilih " required>
                                    <option value ='Y' >Y</option>
                                    <option value ='N' selected >N</option>
                                </select>
                            </div>
                        </div> 

                        <div class="form-group">
                            <label class="control-label col-xs-3" >Neraca </label>
                            <div class="col-xs-9">
                                <select  name="neraca" class="selectpicker show-tick form-control"  title="Pilih " style="width:60%;max-width:50%;" placeholder="Pilih " required>
                                    <option value ='N' selected >Neraca</option>
                                    <option value ='L' >LabaRugi</option>
                                </select>
                            </div>
                        </div> 

                        <div class="form-group">
                            <label class="control-label col-xs-3" >cashBank</label>
                            <div class="col-xs-9">
                                <select  name="cashBank" class="selectpicker show-tick form-control"  title="Pilih " style="width:60%;max-width:50%;" placeholder="Pilih " required>
                                    <option value ='Y' >Y</option>
                                    <option value ='N' selected >N</option>
                                </select>

                                <!-- <input name="cashBank" class="form-control" type="text" style="width:280px;" required> -->
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-3" >rel</label>
                            <div class="col-xs-9">
                                <!-- <input name="rel" class="form-control" type="text" style="width:280px;" required> -->
                                <select  name="rel" class="selectpicker show-tick form-control"  title="Pilih " style="width:60%;max-width:50%;" placeholder="Pilih " required>
                                    <option value ='Y' >Y</option>
                                    <option value ='N' selected >N</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-3" >Aktif </label>
                            <div class="col-xs-9">
                                <select  name="aktif" class="selectpicker show-tick form-control"  title="Pilih " style="width:60%;max-width:50%;" placeholder="Pilih " required>
                                    <option value ='Y' selected >Y</option>
                                    <option value ='N' >N</option>
                                </select>
                            </div>
                        </div> 

                </div>

                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-info">Simpan</button>
                </div>
            </form>
            </div>
            </div>
        </div>

        <!-- ============ MODAL EDIT =============== -->
        <?php
            foreach ($data->result_array() as $coa) {
                $kode=$coa['kode_perkiraan'];
                $nama=$coa['nama_perkiraan'];
                $kelompok=$coa['kelompok'];
                $namaKelompok=$coa['nama_kelompok'];
                $hheader=$coa['hheader'];
                $jenis=$coa['jenis'];
                $neraca=$coa['neraca'];
                $cashBank=$coa['cash_bank'];
                $rel=$coa['rel'];
                $aktif=$coa['aktif'];
                $coa_property=$coa['coa_property'];
            ?>
                <div id="modalEditCoa<?php echo $kode?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 class="modal-title" id="myModalLabel">Edit</h3>
                    </div>
                    <form class="form-horizontal" method="post" action="<?php echo base_url().'admin/Coa/edit'?>">

                        <div class="modal-body">
                            <input name="kode" type="hidden" value="<?php echo $kode;?>">
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-3" >Nama</label>
                            <div class="col-xs-9">
                                <input name="nama" class="form-control" type="text" value="<?php echo $nama;?>" style="width:280px;" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-3" >kelompok</label>
                            <div class="col-xs-9">
                                <input name="kelompok" class="form-control" type="text" value="<?php echo $kelompok;?>" style="width:280px;" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-3" >namaKelompok</label>
                            <div class="col-xs-9">
                                <input name="namaKelompok" class="form-control" type="text" value="<?php echo $namaKelompok;?>" style="width:280px;" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-3" >coa_property</label>
                            <div class="col-xs-9">
                                <input name="coa_property" class="form-control" type="text" value="<?php echo $coa_property;?>" style="width:280px;" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-3" >hheader</label>
                            <div class="col-xs-9">
                                <input name="hheader" class="form-control" type="text" value="<?php echo $hheader;?>" style="width:280px;" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-3" >Jenis </label>
                            <div class="col-xs-9">
                                <select  name="jenis" class="selectpicker show-tick form-control"  title="Pilih Status" style="width:60%;max-width:50%;" placeholder="Pilih Satuan" required>
                                    <option value ='D' <?=$jenis== 'D' ? ' selected="selected"' : ''; ?> >Debet</option>
                                    <option value ='K' <?=$jenis== 'D' ? ' selected="selected"' : ''; ?> >Kredit</option>
                                </select>
                            </div>
                        </div> 

                        <div class="form-group">
                            <label class="control-label col-xs-3" >cust </label>
                            <div class="col-xs-9">
                                <!-- <input name="cust" class="form-control" type="text" value="<?php echo $cust;?>" style="width:280px;" required> -->
                                <select  name="cust" class="selectpicker show-tick form-control"  title="Pilih " style="width:60%;max-width:50%;" placeholder="Pilih " required>
                                    <option value ='Y' <?=$cust== 'Y' ? ' selected="selected"' : ''; ?> >Y</option>
                                    <option value ='N' <?=$cust== 'N' ? ' selected="selected"' : ''; ?> >N</option>
                                </select>
                            </div>
                        </div> 
                        
                        <div class="form-group">
                            <label class="control-label col-xs-3" >Neraca </label>
                            <div class="col-xs-9">
                                <select  name="neraca" class="selectpicker show-tick form-control"  title="Pilih " style="width:60%;max-width:50%;" placeholder="Pilih " required>
                                    <option value ='N' <?=$jenis== 'N' ? ' selected="selected"' : ''; ?> >Neraca</option>
                                    <option value ='L' <?=$jenis== 'L' ? ' selected="selected"' : ''; ?> >LabaRugi</option>
                                </select>
                            </div>
                        </div> 

                        <div class="form-group">
                            <label class="control-label col-xs-3" >cashBank</label>
                            <div class="col-xs-9">
                                <!-- <input name="cashBank" class="form-control" type="text" value="<?php echo $cashBank;?>" style="width:280px;" required> -->
                                <select  name="cashBank" class="selectpicker show-tick form-control"  title="Pilih " style="width:60%;max-width:50%;" placeholder="Pilih " required>
                                    <option value ='Y' <?=$cashBank== 'Y' ? ' selected="selected"' : '';?> >Y</option>
                                    <option value ='N' <?=$cashBank== 'N' ? ' selected="selected"' : '';?> >N</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-3" >rel</label>
                            <div class="col-xs-9">
                                <!-- <input name="rel" class="form-control" type="text" value="<?php echo $rel;?>" style="width:280px;" required> -->
                                <select  name="rel" class="selectpicker show-tick form-control"  title="Pilih " style="width:60%;max-width:50%;" placeholder="Pilih " required>
                                    <option value ='Y' <?=$rel== 'Y' ? ' selected="selected"' : '';?> >Y</option>
                                    <option value ='N' <?=$rel== 'N' ? ' selected="selected"' : '';?> >N</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-xs-3" >Aktif </label>
                            <div class="col-xs-9">
                                <select  name="aktif" class="selectpicker show-tick form-control"  title="Pilih " style="width:60%;max-width:50%;" placeholder="Pilih " required>
                                    <option value ='Y' <?=$aktif== '1' ? ' selected="selected"' : '';?>>Y</option>
                                    <option value ='N' <?=$aktif== '0' ? ' selected="selected"' : '';?>>N</option>
                                </select>
                            </div>
                        </div> 
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                            <button type="submit" class="btn btn-info">Update</button>
                        </div>
                    </form>
                </div>
                </div>
                </div>
            <?php
        }
        ?>

        <!-- ============ MODAL HAPUS =============== -->
        <?php
            foreach ($data->result_array() as $coa) {
                $kode=$coa['kode_perkiraan'];
                $nama=$coa['nama_perkiraan'];
            ?>
                <div id="modalHapusBank<?php echo $kode?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 class="modal-title" id="myModalLabel">Hapus </h3>
                    </div>
                    <form class="form-horizontal" method="post" action="<?php echo base_url().'admin/bank/hapus_bank'?>">
                        <div class="modal-body">
                            <p>Yakin mau menghapus data..?</p>
                                    <input name="kode" type="hidden" value="<?php echo $kode; ?>">
                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                            <button type="submit" class="btn btn-primary">Hapus</button>
                        </div>
                    </form>
                </div>
                </div>
                </div>
            <?php
        }
        ?>

        <!--END MODAL-->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p style="text-align:center;">Copyright &copy; <?php echo '2017';?> bybybyby</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="<?php echo base_url().'assets/js/jquery.js'?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url().'assets/js/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dataTables.bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/jquery.dataTables.min.js'?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#mydata').DataTable();
        } );
    </script>
    
</body>

</html>

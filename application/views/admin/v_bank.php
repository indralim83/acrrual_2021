<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Produk By Mfikri.com">
    <meta name="author" content="M Fikri Setiadi">

    <title>Welcome To Point of Sale Apps</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url().'assets/css/bootstrap.min.css'?>" rel="stylesheet">
	<link href="<?php echo base_url().'assets/css/style.css'?>" rel="stylesheet">
	<link href="<?php echo base_url().'assets/css/font-awesome.css'?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url().'assets/css/4-col-portfolio.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/dataTables.bootstrap.min.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/jquery.dataTables.min.css'?>" rel="stylesheet">

</head>

<body>

    <!-- Navigation -->
   <?php 
        $this->load->view('admin/menu');
   ?>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Bank
                    <div class="pull-right"><a href="#" class="btn btn-sm btn-success" data-toggle="modal" data-target="#largeModal"><span class="fa fa-plus"></span> Tambah </a></div>
                </h1>
            </div>
        </div>
        <!-- /.row -->
        <!-- Projects Row -->
        <div class="row">
            <div class="col-lg-12">
            <table class="table table-bordered table-condensed" style="font-size:11px;" id="mydata">
                <thead>
                    <tr>
                        <th style="text-align:center;width:40px;">Code</th>
                        <th>Bank</th>
                        <th style="width:140px;text-align:center;">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                <?php 
                    $no=0;
                    foreach ($data->result_array() as $bank):
                        $no++;
                        $code=$bank['code'];
                        $nm=$bank['name'];
                ?>
                    <tr>
                        <td style="text-align:center;"><?php echo $code;?></td>
                        <td><?php echo $nm;?></td>
                        <td style="text-align:center;">
                            <a class="btn btn-xs btn-warning" href="#modalEditBank<?php echo $code?>" data-toggle="modal" title="Edit"><span class="fa fa-edit"></span> Edit</a>
                            <!-- <a class="btn btn-xs btn-danger" href="#modalHapusBank<?php echo $code?>" data-toggle="modal" title="Hapus"><span class="fa fa-close"></span> Hapus</a> -->
                        </td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
            </div>
        </div>
        <!-- /.row -->
        
        <!-- ============ MODAL ADD =============== -->
        <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 class="modal-title" id="myModalLabel">Tambah </h3>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo base_url().'admin/bank/addnew'?>">
                <div class="modal-body">

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Code </label>
                        <div class="col-xs-9">
                            <input name="code" class="form-control" type="text" placeholder="Input Code " style="width:280px;" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Nama </label>
                        <div class="col-xs-9">
                            <input name="name" class="form-control" type="text" placeholder="Input Nama " style="width:280px;" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Bank 1 </label>
                        <div class="col-xs-9">
                            <input name="bank1" class="form-control" type="text" placeholder="Input Code " style="width:280px;" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Bank Property </label>
                        <div class="col-xs-9">
                            <input name="coa_bank_property" class="form-control" type="text" placeholder="Input Nama " style="width:280px;" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Account No </label>
                        <div class="col-xs-9">
                            <input name="account_no" class="form-control" type="text" placeholder="Input Code " style="width:280px;" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Address </label>
                        <div class="col-xs-9">
                            <input name="address" class="form-control" type="text" placeholder="Input Nama " style="width:280px;" required>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Phone </label>
                        <div class="col-xs-9">
                            <input name="phone" class="form-control" type="text" placeholder="Input Code " style="width:280px;" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Fax </label>
                        <div class="col-xs-9">
                            <input name="fax" class="form-control" type="text" placeholder="Input Nama " style="width:280px;" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >contact </label>
                        <div class="col-xs-9">
                            <input name="contact" class="form-control" type="text" placeholder="Input Code " style="width:280px;" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Gl Account </label>
                        <div class="col-xs-9">
                            <input name="gl_account" class="form-control" type="text" placeholder="Input Nama " style="width:280px;" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >AP Account  </label>
                        <div class="col-xs-9">
                            <input name="ap_account" class="form-control" type="text" placeholder="Input Code " style="width:280px;" required>
                        </div>
                    </div>

                    <!-- <div class="form-group">
                        <label class="control-label col-xs-3" >Aktif </label>
                        <div class="col-xs-9">
                            <input name="aktif" class="form-control" type="text" placeholder="Input Nama " style="width:280px;" required>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label class="control-label col-xs-3" >Aktif </label>
                        <div class="col-xs-9">
                            <select name="aktif" class="selectpicker show-tick form-control" data-live-search="true" title="Pilih Status" data-width="80%" placeholder="Pilih Satuan" required>
                                <option value ='1' selected >Y</option>
                                <option value ='0' >N</option>
                            </select>
                        </div>
                    </div> 

                </div>

                <div class="modal-footer">
                    <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                    <button class="btn btn-info">Simpan</button>
                </div>
            </form>
            </div>
            </div>
        </div>

        <!-- ============ MODAL EDIT =============== -->
        <?php
            foreach ($data->result_array() as $bank) {
                $code=$bank['code'];
                $name=$bank['name'];
                $bank1=$bank['bank1'];
                $coa_bank_property=$bank['coa_bank_property'];
                $account_no=$bank['account_no'];
                $address=$bank['address'];
                $phone=$bank['phone'];
                $fax=$bank['fax'];
                $contact=$bank['contact'];
                $gl_account=$bank['gl_account'];
                $ap_account=$bank['ap_account'];
                $aktif=$bank['aktif'];
            ?>
                <div id="modalEditBank<?php echo $code?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 class="modal-title" id="myModalLabel">Edit</h3>
                    </div>
                    <form class="form-horizontal" method="post" action="<?php echo base_url().'admin/bank/edit'?>">
                        <div class="modal-body">
                            <input name="code" type="hidden" value="<?php echo $code;?>">

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Nama</label>
                        <div class="col-xs-9">
                            <input name="name" class="form-control" type="text" value="<?php echo $name;?>" style="width:280px;" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >bank1</label>
                        <div class="col-xs-9">
                            <input name="bank1" class="form-control" type="text" value="<?php echo $bank1;?>" style="width:280px;" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >coa_bank_property</label>
                        <div class="col-xs-9">
                            <input name="coa_bank_property" class="form-control" type="text" value="<?php echo $coa_bank_property;?>" style="width:280px;" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >account_no</label>
                        <div class="col-xs-9">
                            <input name="account_no" class="form-control" type="text" value="<?php echo $account_no;?>" style="width:280px;" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >address</label>
                        <div class="col-xs-9">
                            <input name="address" class="form-control" type="text" value="<?php echo $address;?>" style="width:280px;" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >phone</label>
                        <div class="col-xs-9">
                            <input name="phone" class="form-control" type="text" value="<?php echo $phone;?>" style="width:280px;" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >fax</label>
                        <div class="col-xs-9">
                            <input name="fax" class="form-control" type="text" value="<?php echo $fax;?>" style="width:280px;" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >contact</label>
                        <div class="col-xs-9">
                            <input name="contact" class="form-control" type="text" value="<?php echo $contact;?>" style="width:280px;" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >gl_account</label>
                        <div class="col-xs-9">
                            <input name="gl_account" class="form-control" type="text" value="<?php echo $gl_account;?>" style="width:280px;" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-xs-3" >ap_account</label>
                        <div class="col-xs-9">
                            <input name="ap_account" class="form-control" type="text" value="<?php echo $ap_account;?>" style="width:280px;" required>
                        </div>
                    </div>

                    <!-- <div class="form-group">
                        <label class="control-label col-xs-3" >aktif</label>
                        <div class="col-xs-9">
                            <input name="aktif" class="form-control" type="text" value="<?php echo $aktif;?>" style="width:280px;" required>
                        </div>
                    </div> -->

                    <div class="form-group">
                        <label class="control-label col-xs-3" >Aktif </label>
                        <div class="col-xs-9">
                            <select name="aktif" class="selectpicker show-tick form-control"  title="Pilih Status" data-width="80%" placeholder="Pilih Satuan" required>
                                <option value ='1' <?=$aktif== '1' ? ' selected="selected"' : '';?> >Y</option>
                                <option value ='0' <?=$aktif== '0' ? ' selected="selected"' : '';?>>N</option>
                            </select>
                        </div>
                    </div> 

                   

                </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                            <button type="submit" class="btn btn-info">Update</button>
                        </div>
                    </form>
                </div>
                </div>
                </div>
            <?php
        }
        ?>

        <!-- ============ MODAL HAPUS =============== -->
        <?php
            foreach ($data->result_array() as $bank) {
                $code=$bank['code'];
                $nm=$bank['name'];
            ?>
                <div id="modalHapusBank<?php echo $code?>" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="largeModal" aria-hidden="true">
                    <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 class="modal-title" id="myModalLabel">Hapus </h3>
                    </div>
                    <form class="form-horizontal" method="post" action="<?php echo base_url().'admin/bank/hapus_bank'?>">
                        <div class="modal-body">
                            <p>Yakin mau menghapus data..?</p>
                                    <input name="kode" type="hidden" value="<?php echo $code; ?>">
                        </div>
                        <div class="modal-footer">
                            <button class="btn" data-dismiss="modal" aria-hidden="true">Tutup</button>
                            <button type="submit" class="btn btn-primary">Hapus</button>
                        </div>
                    </form>
                </div>
                </div>
                </div>
            <?php
        }
        ?>

        <!--END MODAL-->

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p style="text-align:center;">Copyright &copy; <?php echo '2017';?> bybybyby</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="<?php echo base_url().'assets/js/jquery.js'?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url().'assets/js/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dataTables.bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/jquery.dataTables.min.js'?>"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#mydata').DataTable();
        } );
    </script>
    
</body>

</html>

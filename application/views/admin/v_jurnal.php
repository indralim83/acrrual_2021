<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Produk By Mfikri.com">
    <meta name="author" content="M Fikri Setiadi">

    <title>Back Office System</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url().'assets/css/bootstrap.min.css'?>" rel="stylesheet">
	<link href="<?php echo base_url().'assets/css/style.css'?>" rel="stylesheet">
	<link href="<?php echo base_url().'assets/css/font-awesome.css'?>" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url().'assets/css/4-col-portfolio.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/dataTables.bootstrap.min.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/css/jquery.dataTables.min.css'?>" rel="stylesheet">
    <link href="<?php echo base_url().'assets/dist/css/bootstrap-select.css'?>" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/bootstrap-datetimepicker.min.css'?>">
</head>

<body>

    <!-- Navigation -->
   <?php 
        $this->load->view('admin/menu');
   ?>

    <!-- Page Content -->
    <div class="container">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
            <center><?php echo $this->session->flashdata('msg');?></center>
                <h1 class="page-header">JURNAL
                    <small>UMUM v1</small>
                    
                </h1>
            </div>
        </div>
        <!-- /.row -->
        <!-- Projects Row -->
        <div class="row">
            <div class="col-lg-12">
            <form action="" method="post"> <!--<?php echo base_url().'admin/jurnal/add_to_cart'?>-->
            <table>
                <tr>
                    <th style="width:100px;padding-bottom:5px;">No Urut</th>
                    <th style="width:300px;padding-bottom:5px;">
                    	<input type="text" name="nourut" id="nourut" value="" class="form-control input-sm" style="width:200px;"  disabled>
                    </th>

                    <th style="width:90px;padding-bottom:5px;">No Voucher</th>
                    <td style="width:350px;">
                        <input type="text" name="novoucher" id="novoucher" value="" class="form-control input-sm" style="width:200px;" required>                       
                    </td>
                    <th>Jenis Voucher</th>
                    <td>
                          <select name="jenis" id="jenis" class="selectpicker show-tick form-control" data-live-search="true" title="Pilih Suplier" data-width="75%" required>
                          <?php 
                                echo "<option value='1' selected>Transaksi Penerimaan</option>";
                                echo "<option value='2'>Transaksi Pengeluaran</option>";
                                echo "<option value='3'>Transaksi Pemindahan</option>";
                            ?>
                    </select>
                        
                    </td>
                    
                </tr>
                <tr>
                 
                    <th>Issue Date</th>
                    <td>
                        <div class='input-group date' id='datepicker' style="width:200px;">
                            <input type='text' name="tglissue" id="tglissue" class="form-control" value="<?php echo $this->session->userdata('issue_date');?>" placeholder="Tanggal..." required/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </td>
                    <th>Book Date</th>
                    <td>
                        <div class='input-group date' id='datepicker1' style="width:200px;">
                            <input type='text' name="tglbook" id="tglbook" class="form-control" value="<?php echo $this->session->userdata('booking_date');?>" placeholder="Tanggal..." required/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </td>
                    <th style="width:90px;padding-bottom:5px;">Dibayarkan Kepada</th>
                    <td style="width:350px;">
                   		<input type="text" name="dibayarkpd" id="dibayarkpd" placeholder="dibayar kepada" class="form-control input-sm" style="width:500px;" required> 
                    </td>
                </tr>
            </table><hr/>

            <table>
            	<tr>
            		<div id="detail_coa" style="position:absolute;">
            
            		</div>
            	</tr>
                <tr>
                    <th style="width:20%" align="center">Kode COA</th>
                    <th style="width:20%" align="center">BAGIAN</th>
                    <th style="width:200%" align="center">KETERANGAN</th>
                   	<th>BANK/KAS</th>
		            <th>REL</th>
		            <th>CUST/SPL</th>
		            <th>DEBET</th>
		            <th>KREDIT</th>
                </tr>

                <tr>
                    <th>
                    <div style="width:200px;"> <!--overflow: hidden; -->
                    <select name="kode_coa" id="kode_coa" class="selectpicker show-tick form-control" data-live-search="true" title="COA" data-width="100%" required>
                        <?php foreach ($coa->result_array() as $i) {
                            $id=$i['kode_perkiraan'];
                            $nm=$i['coa_property'];
                            $des=$i['nama_perkiraan'];
                            $sess_id=$this->session->userdata('suplier');
                            if($sess_id==$id_sup)
                                echo "<option value='$id' >$nm - $des</option>";
                            else
                                echo "<option value='$id'>$nm - $des </option>";
                        }?>
                    </select>

                    </div>

                    </th>
                    <th>
                    	<div style="width:100px;"> <!--overflow: hidden; -->
                    	<select name="bagian" id="bagian" class="selectpicker show-tick form-control" data-live-search="true" title="Bagian" data-width="100%" required>
                        <?php foreach ($bagian->result_array() as $i) {
                            $id=$i['code_dept'];
                            $nm=$i['code_dept'];
                            $des=$i['name_dept'];
                            $sess_id=$this->session->userdata('suplier');
                            if($sess_id==$id_sup)
                                echo "<option value='$id'>$nm - $des</option>";  //selected
                            else
                                echo "<option value='$id'>$nm - $des </option>";
                        }?>
                    	</select>
                    	</div>
                    </th>
                    <th>

                    	<input type="text" name="keterangan" id="keterangan" value="" placeholder="keterangan" class="form-control input-sm" style="width:500px;" required> 
                    </th>
                    <th>
                    	<div style="width:180px;"> <!--overflow: hidden; -->
						<select name="bank" id="bank" class="selectpicker show-tick form-control" data-live-search="true" title="Kas/Bank" data-width="100%" >
			                        <?php foreach ($bank->result_array() as $i) {
			                            $id=$i['code'];
			                            $nm=$i['bank1'];
			                            $des=$i['account_no'];
			                            $sess_id=$this->session->userdata('suplier');
			                            if($sess_id==$id_sup)
			                                echo "<option value='$id'>$id - $nm - $des</option>";
			                            else
			                                echo "<option value='$id'>$id - $nm - $des </option>";
			                        }?>
                    	</select>
                    	</div>
                    </th>
                    <th>

                    	<input type="text" name="rel" id="rel" value="" placeholder="Rel No" title="rel" class="form-control input-sm" style="width:100px;"> 
                    	
                    </th>
                    <th>
                    	<div style="width:100px;"> <!--overflow: hidden; -->
                    	<select name="suplier" id="cust" class="selectpicker show-tick form-control" data-live-search="true" title="Supplier" data-width="100%">
			                        <?php foreach ($sup->result_array() as $i) {
			                            $id_sup=$i['id_spl'];
			                            $nm_sup=$i['kode_supplier'];
			                            $al_sup=$i['nama_supplier'];
			                            $notelp_sup=$i['alamat'];
			                            $sess_id=$this->session->userdata('suplier');
			                            if($sess_id==$id_sup)
			                                echo "<option value='$id_sup'>$nm_sup - $al_sup</option>";
			                            else
			                                echo "<option value='$id_sup'>$nm_sup - $al_sup </option>";
			                        }?>
			            </select>
			        	</div>
                    </th>
                    <th>  <input type="text" name="debet" id="debet" placeholder="Debet" class="form-control input-sm" style="width:120px;" required> </th>
		                  <td>  <input type="text" name="kredit"  id="kredit"  placeholder="kredit" class="form-control input-sm" style="width:120px;" required> 
		             <th><!-- <button type="" id="simpan" class="btn btn-sm btn-primary">Ok</button> -->
                     <a href="#" title="Simpan" id="simpan" class="btn btn-sm btn-primary">OK</a>
                    </th>
                </tr>		

            </table>

             </form>
            <table class="table table-bordered table-condensed" style="font-size:11px;margin-top:10px;">
                <thead>
                    <tr>
                        <th>KODE PERKIRAAN</th>
                        <th>NAMA PERKIRAAN</th>
                        <th style="text-align:center;">BAGIAN</th>
                        <th style="text-align:center;">KETERANGAN</th>
                        <th style="text-align:center;">BANK/KAS</th>
                        <th style="text-align:center;">REL</th>
                        <th style="text-align:center;">CUST</th>
                        <th style="text-align:right;">DEBET</th>
                        <th style="text-align:right;">KREDIT</th>
                        <th style="width:100px;text-align:center;">AKSI</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1; ?>
                    <?php foreach ($this->cart->contents() as $items): ?>
                    <?php echo form_hidden($i.'[rowid]', $items['rowid']); ?>
                    <tr>
                         <td><?=$items['id'];?></td>
                         <td><?=$items['name'];?></td>
                         <td style="text-align:center;"><?=$items['satuan'];?></td>
                         <td style="text-align:right;"><?php echo number_format($items['price']);?></td>
                         <td style="text-align:right;"><?php echo number_format($items['harga']);?></td>
                         <td style="text-align:center;"><?php echo number_format($items['qty']);?></td>
                         <td style="text-align:right;"><?php echo number_format($items['subtotal']);?></td>
                         <td style="text-align:center;"><a href="<?php echo base_url().'admin/pembelian/remove/'.$items['rowid'];?>" class="btn btn-warning btn-xs"><span class="fa fa-close"></span> Batal</a></td>
                    </tr>
                    <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="7" style="text-align:center;">Total</td>
                        <td style="text-align:right;">Rp. <?php echo number_format($this->cart->total());?></td>
                        <td style="text-align:right;">Rp. <?php echo number_format($this->cart->total());?></td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>
            
            </div>
        </div>
        <!-- /.row -->
        

        <hr>

        <!-- Footer -->
        <footer>
            <div class="row">
                <div class="col-lg-12">
                    <p style="text-align:center;">Copyright &copy; <?php echo '2017';?> by me</p>
                </div>
            </div>
            <!-- /.row -->
        </footer>

    </div>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="<?php echo base_url().'assets/js/jquery.js'?>"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url().'assets/dist/js/bootstrap-select.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/dataTables.bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/jquery.dataTables.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/jquery.price_format.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/moment.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/bootstrap-datetimepicker.min.js'?>"></script>


    <script type="text/javascript">
			 $(function () {
                $('#datetimepicker').datetimepicker({
                    format: 'DD MMMM YYYY HH:mm',
                });
                
                $('#datepicker').datetimepicker({
                    format: 'YYYY-MM-DD',
                });
                $('#datepicker1').datetimepicker({
                    format: 'YYYY-MM-DD',
                });

                $('#timepicker').datetimepicker({
                    format: 'HH:mm'
                });
            });
    </script>
    <script type="text/javascript">
        $(function(){
            $('.harpok').priceFormat({
                    prefix: '',
                    //centsSeparator: '',
                    centsLimit: 0,
                    thousandsSeparator: ','
            });
            $('.harjul').priceFormat({
                    prefix: '',
                    //centsSeparator: '',
                    centsLimit: 0,
                    thousandsSeparator: ','
            });
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function(){


        $("#debet").keyup(function(){
            cek_Debet();
        });
        $("#kredit").keyup(function(){
            cek_kredit();
        });

        function cek_Debet()
        {
            var debet=$('#debet').val();
            if (debet.length==0)
            {
               
            }
            else
            {
               
                $("#kredit").val('0');  
            }
        }

        function cek_kredit()
        {
            var kredit=$('#kredit').val();
            if (kredit.length==0)
            {
            }
            else
            {
                $("#debet").val('0');   
                
            }
        }

        $("#simpan").click(function(){
            simpan_header();
        });
		
        function simpan_header()
		{
			
			var nourut				= $("#nourut").val();
			var novoucher			= $("#novoucher").val();
			var tglissue			= $("#tglissue").val();
			var tglbook			    = $("#tglbook").val();
	        var jenis         		= $("#jenis").val();
			var dibayarkpd			= $("#dibayarkpd").val();
			
			//if(tgl_issue.length==0){
			//	alert('Maaf, Tanggal tidak boleh kosong');
	        //    $("#tgl_issue").focus();
		    //    return false();
			//}
			        
			$.ajax({
						type	 : "POST",
						url		 : "<?php echo base_url().'admin/jurnal/simpanheader';?>",
						data	 : "nourut="+nourut+"&tglissue="+tglissue+"&tglbook="+tglbook+"&jenis="+jenis+"&novoucher="+novoucher+"&dibayar="+dibayarkpd,
			            dataType : "json",		
						success	: function(data){
			                $("#nourut").val(data.Pkode);
						}
					});

            var nourut				= "AD210000111"; //$("#nourut").val();
			var kode_coa			= $("#kode_coa").val();
			var bagian   			= $("#bagian").val();
			var keterangan		    = $("#keterangan").val();
	        var bank				= $("#bank").val();
			var rel      			= $("#rel").val();
			var cust	    		= $("#cust").val();
			var debet	 		    = $("#debet").val();
	        var kredit	     		= $("#kredit").val();
			
			simpan_detailP(nourut,kode_coa,bagian,keterangan,bank,rel,cust,debet,kredit)
		}
					
		function simpan_detailP(nourut,coa,bagian,keterangan,bank,rel,cust,debet,kredit)
		{
//
					$.ajax({
						type	 : "POST",
						url		 : "<?php echo base_url().'admin/jurnal/simpandetail';?>",
						data	 : "nourut="+nourut+"&kode_coa="+coa+"&kode_bagian="+bagian+"&keterangan="+keterangan+"&kas_bank="+bank+"&rel="+rel+"&cust="+cust+"&debet="+debet+"&kredit="+kredit,
			            dataType : "json",		
						success	: function(data){
			               // $("#nourut").val(data.Pkode);
						}
					});

		}
		

        function simpan_detail()
        {
        	var Prel	    	= $("#rel").val();
			var Pprel	    	= $("#pprel").val();
			var Pbank			= $("#bank").val();
			var Ppbank			= $("#ppcash_bank").val();
			var Pcust			= $("#cust").val();
			var Ppcust			= $("#ppcust").val();
			

        	if(Pprel.length>0){
        		if(Prel.length==0){
					if (Pprel=='Y')	
	        		{	
						alert('Maaf, Kode Rel tidak boleh kosong, silahkan diisi kembali');
						$("#rel").focus();
						return false();
					}
				}
			}

			if(Ppbank.length>0){
        		if(Pbank.length==0){
					if (Ppbank=='Y')	
	        		{	
						alert('Maaf, Kode Bank tidak boleh kosong, silahkan diisi kembali');
						$("#bank").focus();
						return false();
					}
				}
			}
			if(Ppcust.length>0){
        		if(Pcust.length==0){
					if (Ppcust=='Y')	
	        		{	
						alert('Maaf, Kode Cust / Supplier tidak boleh kosong, silahkan diisi kembali');
						$("#cust").focus();
						return false();
					}
				}
			}


        }
            //Ajax kabupaten/kota insert
				$("#debet").keypress(function(data){
					if (data.which!=8 && data.which!=0 && data.which!=46 && data.which!=45 && (data.which<48 || data.which>57) ) {
						return false;
					}
				});
				$("#kredit").keypress(function(data){
					if (data.which!=8 && data.which!=0 && data.which!=46 && (data.which<48 || data.which>57) ) {
						return false;
					}
				});
					
			    $("#debet").keyup(function(){
					cek_Debet();
				});
			    $("#kredit").keyup(function(){
					cek_kredit();
				});
			    function cek_Debet()
			    {
			        var debet=$('#debet').val();
			        if (debet.length==0)
			        {
			           
			        }
			        else
			        {
			           
			            $("#kredit").val('0');	
			        }
			    }
			    function cek_kredit()
			    {
			        var kredit=$('#kredit').val();
			        if (kredit.length==0)
			        {
			        }
			        else
			        {
			            $("#debet").val('0');	
			            
			        }
			    }


            $("#kode_coa").focus();
            $("#kode_coa").change(function(){
                var kobar = {kode_coa:$(this).val()};
                   $.ajax({
               type: "POST",
               url : "<?php echo base_url().'admin/jurnal/get_coa';?>",
               data: kobar,
               success: function(msg){
               $('#detail_coa').html(msg);
               }
            });
            }); 

            $("#kode_brg").keypress(function(e){
                if(e.which==13){
                    $("#jumlah").focus();
                }
            });
        });
    </script>
    
</body>

</html>

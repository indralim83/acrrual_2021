<?php
class Bank extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url();
            redirect($url);
        };
		$this->load->model('m_bank');
    }

    function index(){
        if($this->session->userdata('akses')=='1'){
            $data['data']=$this->m_bank->tampil_();
            $this->load->view('admin/v_bank',$data);
        }else{
            echo "Halaman tidak ditemukan";
        }
    }

    function addnew(){
        if($this->session->userdata('akses')=='1'){
            $code=$this->input->post('code');
            $name=$this->input->post('name');
            $bank1=$this->input->post('bank1');
            $coa_bank_property=$this->input->post('coa_bank_property');
            $account_no=$this->input->post('account_no');
            $address=$this->input->post('address');
            $phone=$this->input->post('phone');
            $fax=$this->input->post('fax');
            $contact=$this->input->post('contact');
            $gl_account=$this->input->post('gl_account');
            $ap_account=$this->input->post('ap_account');
            $aktif=$this->input->post('aktif');
            $hasilQuery = $this->m_bank->simpan_bank($code,$name,$bank1,$coa_bank_property,$account_no,$address,$phone,$fax,$contact,$gl_account,$ap_account,$aktif);
            echo "save bank => " , $hasilQuery;
            if ($hasilQuery == '1') {
                redirect('admin/bank');
                return ;
            }
            echo "Error insert to database ", $hasilQuery;
        }else{
            echo "Halaman tidak ditemukan";
        }
    }

    function edit(){
        if($this->session->userdata('akses')=='1'){
            $code=$this->input->post('code');
            $name=$this->input->post('name');
            $bank1=$this->input->post('bank1');
            $coa_bank_property=$this->input->post('coa_bank_property');
            $account_no=$this->input->post('account_no');
            $address=$this->input->post('address');
            $phone=$this->input->post('phone');
            $fax=$this->input->post('fax');
            $contact=$this->input->post('contact');
            $gl_account=$this->input->post('');
            $ap_account=$this->input->post('ap_account');
            $aktif=$this->input->post('aktif');
            $hasilQuery = $this->m_bank->update_bank($code,$name,$bank1,$coa_bank_property,$account_no,$address,$phone,$fax,$contact,$gl_account,$ap_account,$aktif);
            echo "save bank => " , $hasilQuery;
            if ($hasilQuery == '1') {
                redirect('admin/bank');
                return ;
            }
            echo "Error update to database ", $hasilQuery;
        }else{
            echo "Halaman tidak ditemukan";
        }
    }

}
<?php
class Coa extends CI_Controller{

    function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url();
            redirect($url);
        };
		$this->load->model('m_coa');
    }
    
    function index() {
        if($this->session->userdata('akses')=='1'){
            $data['data']=$this->m_coa->getAllCoa();
            $this->load->view('admin/v_coa',$data);
        }else{
            echo "Halaman tidak ditemukan";
        }
    }

    function addnew(){

        if($this->session->userdata('akses')=='1'){
            $kode=$this->input->post('kode');
            $nama=$this->input->post('nama');
            $kelompok=$this->input->post('kelompok');
            $namaKelompok=$this->input->post('namaKelompok');
            $hheader=$this->input->post('hheader');
            $jenis=$this->input->post('jenis');
            $neraca=$this->input->post('neraca');
            $cashBank=$this->input->post('cashBank');
            $rel=$this->input->post('rel');
            $cust=$this->input->post('cust');
            $coa_property=$this->input->post('coa_property');
            $aktif=$this->input->post('aktif');

            $log = '
                <script>
                console.log(kode =' . json_encode($kode, JSON_HEX_TAG). '); 
                console.log(nama =' . json_encode($nama, JSON_HEX_TAG). '); 
                </script>';
            echo "save bank => " . $log;
            echo "save bank => " . $nama;
            $hasilQuery = $this->m_coa->simpan_coa(
                $kode, $coa_property, $nama, $kelompok, $namaKelompok, $hheader, $jenis, $rel, $cust, $cashBank, $neraca, $aktif
            );
            echo "save bank => " , $hasilQuery;
            if ($hasilQuery == '1') {
                redirect('admin/Coa');
                return ;
            }
            echo "Error insert to database ", $hasilQuery;
        }else{
            echo "Halaman tidak ditemukan";
        }
    }

    function edit(){

        if($this->session->userdata('akses')=='1'){
            $kode=$this->input->post('kode');
            $nama=$this->input->post('nama');
            $kelompok=$this->input->post('kelompok');
            $namaKelompok=$this->input->post('namaKelompok');
            $hheader=$this->input->post('hheader');
            $jenis=$this->input->post('jenis');
            $neraca=$this->input->post('neraca');
            $cashBank=$this->input->post('cashBank');
            $rel=$this->input->post('rel');
            $cust=$this->input->post('cust');
            $coa_property=$this->input->post('coa_property');
            $aktif=$this->input->post('aktif');

            $log = '
                <script>
                console.log(kode =' . json_encode($kode, JSON_HEX_TAG). '); 
                console.log(nama =' . json_encode($nama, JSON_HEX_TAG). '); 
                </script>';
            echo "save bank => " . $log;
            echo "save bank => " . $nama;
            $hasilQuery = $this->m_coa->update_coa(
                $kode, $coa_property, $nama, $kelompok, $namaKelompok, $hheader, $jenis, $rel, $cust, $cashBank, $neraca, $aktif
            );
            echo "save bank => " , $hasilQuery;
            if ($hasilQuery == '1') {
                redirect('admin/Coa');
                return ;
            }
            echo "Error insert to database ", $hasilQuery;
        }else{
            echo "Halaman tidak ditemukan";
        }
    }
    
}
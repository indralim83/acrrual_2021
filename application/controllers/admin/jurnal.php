<?php
class Jurnal extends CI_Controller{
	function __construct(){
		parent::__construct();
		if($this->session->userdata('masuk') !=TRUE){
            $url=base_url();
            redirect($url);
        };
		$this->load->model('m_bagian');
		$this->load->model('m_bank');
		$this->load->model('m_suplier');
		$this->load->model('m_jurnal');
		$this->load->model('m_coa');

		//$this->output->enable_profiler(TRUE);
		
	}
	function index(){
	if($this->session->userdata('akses')=='1'){
		$x['coa']=$this->m_coa->tampil_coa();
		$x['bagian']=$this->m_bagian->tampil_();
		$x['bank']=$this->m_bank->tampil_();
		$x['sup']=$this->m_suplier->tampil_();
	
		$this->load->view('admin/v_jurnal',$x);
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	function get_coa(){
		if($this->session->userdata('akses')=='1'){
			$kobar=$this->input->post('kode_coa');
			$x['coa']=$this->m_coa->get_coa($kobar);
		
			$this->load->view('admin/v_detail_coa',$x);
		}else{
	        echo "Halaman tidak ditemukan";
	    }
	}

	function simpanheader(){
		if($this->session->userdata('akses')=='1')
		{
			$novoucher=$this->input->post('novoucher');
			$tglissue=$this->input->post('tglissue');
			$tglbook=$this->input->post('tglbook');
			$jenis=$this->input->post('jenis');
			$dibayar=$this->input->post('dibayar');
			if(!empty($tglissue) && !empty($tglbook))
			{
				$user=$this->session->userdata('user');
				$jurnal_kode=$this->m_jurnal->get_nojurnal($user);
				$data['Pkode']=$jurnal_kode;
		    	echo json_encode($data);

				$order_proses=$this->m_jurnal->simpan_header($jurnal_kode,$tglissue,$tglbook,$novoucher,$jenis,$dibayar);
			//	echo $this->session->set_flashdata('msg','<label class="label label-success">Jurnal Berhasil di Simpan ke Database</label>');
					
			}		
		}
	}
	function simpandetail(){
		if($this->session->userdata('akses')=='1')
		{

			$nourut=$this->input->post('nourut');
			$kode_coa=$this->input->post('kode_coa');
			$kode_bagian=$this->input->post('kode_bagian');
			$keterangan=$this->input->post('keterangan');
			$kas_bank=$this->input->post('kas_bank');
			$rel=$this->input->post('rel');
			$cust=$this->input->post('cust');
			$debet=$this->input->post('debet');
			$kredit=$this->input->post('kredit');
			$username=$this->session->userdata('user');
				


			//if(!empty($tglissue) && !empty($tglbook))
			//{
				//$order_proses=$this->m_jurnal->simpan_detail($nourut);
				$order_proses=$this->m_jurnal->simpan_detail($nourut,$kode_coa,$kode_bagian,$keterangan,$kas_bank,$rel,$cust,$debet,$kredit,$username);
			//	echo $this->session->set_flashdata('msg','<label class="label label-success">Jurnal Berhasil di Simpan ke Database</label>');
					
			//}		
		}
	}
	
	function add_to_cart(){
	if($this->session->userdata('akses')=='1'){
		$nofak=$this->input->post('nofak');
		$tgl=$this->input->post('tgl');
		$suplier=$this->input->post('suplier');
		$this->session->set_userdata('nofak',$nofak);
		$this->session->set_userdata('tglfak',$tgl);
		$this->session->set_userdata('suplier',$suplier);
		$kobar=$this->input->post('kode_brg');
		$produk=$this->m_barang->get_barang($kobar);
		$i=$produk->row_array();
		$data = array(
               'id'       => $i['barang_id'],
               'name'     => $i['barang_nama'],
               'satuan'   => $i['barang_satuan'],
               'price'    => $this->input->post('harpok'),
               'harga'    => $this->input->post('harjul'),
               'qty'      => $this->input->post('jumlah')
            );

		$this->cart->insert($data); 
		redirect('admin/pembelian');
	}else{
        echo "Halaman tidak ditemukan";
    }
	}
	function remove(){
	if($this->session->userdata('akses')=='1'){
		$row_id=$this->uri->segment(4);
		$this->cart->update(array(
               'rowid'      => $row_id,
               'qty'     => 0
            ));
		redirect('admin/pembelian');
	}else{
        echo "Halaman tidak ditemukan";
    }
	}


	
	function simpan_pembelian(){
	if($this->session->userdata('akses')=='1'){
		$nourut=$this->session->userdata('nourut');
		$tgl_issue=$this->session->userdata('tgl_issue');
		$tgl_book=$this->session->userdata('tgl_book');
		if(!empty($nourut) && !empty($tgl_issue) && !empty($tgl_book)){
			$beli_kode=$this->m_pembelian->get_kobel();
			$order_proses=$this->m_pembelian->simpan_pembelian($nofak,$tglfak,$suplier,$beli_kode);
			if($order_proses){
				$this->cart->destroy();
				$this->session->unset_userdata('nofak');
				$this->session->unset_userdata('tglfak');
				$this->session->unset_userdata('suplier');
				echo $this->session->set_flashdata('msg','<label class="label label-success">Pembelian Berhasil di Simpan ke Database</label>');
				redirect('admin/pembelian');	
			}else{
				redirect('admin/pembelian');
			}
		}else{
			echo $this->session->set_flashdata('msg','<label class="label label-danger">Pembelian Gagal di Simpan, Mohon Periksa Kembali Semua Inputan Anda!</label>');
			redirect('admin/pembelian');
		}
	}else{
        echo "Halaman tidak ditemukan";
    }	
	}
}
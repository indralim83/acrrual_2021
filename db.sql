-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.0.27-community-nt - MySQL Community Edition (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             8.0.0.4396
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for db_penjualan
CREATE DATABASE IF NOT EXISTS `db_penjualan` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `db_penjualan`;


-- Dumping structure for table db_penjualan.gl_bank
CREATE TABLE IF NOT EXISTS `gl_bank` (
  `code` char(15) NOT NULL,
  `bank1` varchar(50) default NULL,
  `coa_bank_property` varchar(50) default NULL,
  `name` varchar(50) default NULL,
  `account_no` varchar(50) default NULL,
  `address` varchar(50) default NULL,
  `phone` varchar(50) default NULL,
  `fax` varchar(50) default NULL,
  `contact` varchar(50) default NULL,
  `gl_account` varchar(50) default NULL,
  `ap_account` varchar(50) default NULL,
  `aktif` char(1) default NULL,
  PRIMARY KEY  (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_penjualan.gl_d_voucher
CREATE TABLE IF NOT EXISTS `gl_d_voucher` (
  `nomor` int(10) NOT NULL auto_increment,
  `no_urut` char(15) default NULL,
  `kode_coa` char(15) default NULL,
  `kode_bagian` char(6) default NULL,
  `keterangan` longtext,
  `debet` double default '0',
  `kas_bank` char(15) default NULL,
  `rel` char(15) default NULL,
  `cust` char(15) default NULL,
  `kredit` double default '0',
  `d_k` char(2) default NULL,
  `bl` int(11) default NULL,
  `aun` int(11) default NULL,
  `username` char(10) default NULL,
  `idrec` int(11) default NULL,
  PRIMARY KEY  (`nomor`),
  KEY `no_urut` (`no_urut`),
  KEY `cust` (`cust`),
  KEY `rel` (`rel`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_penjualan.gl_form_payment
CREATE TABLE IF NOT EXISTS `gl_form_payment` (
  `no_urut` char(50) NOT NULL,
  `tanggal` date default NULL,
  `tanggal_pembayaran` date default NULL,
  `kode_supplier` char(50) default NULL,
  `kode_bank` char(50) default NULL,
  `user_name` char(50) default NULL,
  `total` double default NULL,
  `status` int(11) default NULL,
  `code` char(2) default NULL,
  PRIMARY KEY  (`no_urut`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_penjualan.gl_form_voucher_ap
CREATE TABLE IF NOT EXISTS `gl_form_voucher_ap` (
  `idrec` int(11) NOT NULL auto_increment,
  `no_urut` char(15) NOT NULL,
  `tanggal` date default NULL,
  `kode_supplier` char(50) default NULL,
  `nama_supplier` char(50) default NULL,
  `user_name` char(50) default NULL,
  `kode_bagian` char(50) default NULL,
  `kode_transaksi` char(50) default NULL,
  `keterangan` varchar(150) default NULL,
  `rel` char(10) default NULL,
  `cust` char(10) default NULL,
  `total` double default NULL,
  `kode_coa5` char(50) default NULL,
  `kode_coa_ar` char(50) default NULL,
  PRIMARY KEY  (`idrec`),
  KEY `FK_gl_form_voucher_ap_gl_form_voucher_ap_m` (`no_urut`),
  CONSTRAINT `FK_gl_form_voucher_ap_gl_form_voucher_ap_m` FOREIGN KEY (`no_urut`) REFERENCES `gl_form_voucher_ap_m` (`no_urut`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_penjualan.gl_form_voucher_ap_m
CREATE TABLE IF NOT EXISTS `gl_form_voucher_ap_m` (
  `no_urut` char(15) NOT NULL,
  `tanggal` date default NULL,
  `kode_supplier` char(50) default NULL,
  `user_name` char(50) default NULL,
  `total` double default NULL,
  `status` int(11) default NULL,
  `code` char(2) default NULL,
  PRIMARY KEY  (`no_urut`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_penjualan.gl_m_transaksi_ap
CREATE TABLE IF NOT EXISTS `gl_m_transaksi_ap` (
  `id` int(10) NOT NULL auto_increment,
  `kode_tran` char(50) default NULL,
  `nama` char(50) default NULL,
  `gl_cost` char(50) default NULL,
  `gl_ap` char(50) default NULL,
  `status` char(50) default NULL,
  `bagian` char(50) default NULL,
  `code` char(2) default NULL,
  `group_transaksi` char(4) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_penjualan.gl_m_transaksi_ap_group
CREATE TABLE IF NOT EXISTS `gl_m_transaksi_ap_group` (
  `code_group` char(4) default NULL,
  `keterangan` varchar(50) default NULL,
  `aktif` char(1) default NULL,
  `ar_ap` char(2) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_penjualan.gl_m_voucher
CREATE TABLE IF NOT EXISTS `gl_m_voucher` (
  `no_urut` char(15) NOT NULL,
  `issue_date` date default NULL,
  `booking_date` date default NULL,
  `no_voucher` char(6) default NULL,
  `dibayar` varchar(50) default NULL,
  `status` char(1) default NULL,
  `jenis_voucher` char(1) default NULL,
  `tutupbln` char(1) default NULL,
  `bl` char(1) default NULL,
  PRIMARY KEY  (`no_urut`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_penjualan.gl_tbl_ekuitas
CREATE TABLE IF NOT EXISTS `gl_tbl_ekuitas` (
  `tgl` date default NULL,
  `kode_coa` char(15) default NULL,
  `nilai` double default NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_penjualan.gl_to_ar
CREATE TABLE IF NOT EXISTS `gl_to_ar` (
  `tgl` date default NULL,
  `folio` varchar(200) default NULL,
  `saldoawal` double default NULL,
  `debet` double default NULL,
  `kredit` double default NULL,
  `saldoakhir` double default NULL,
  `datein` date default NULL,
  `dateout` date default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_penjualan.groups
CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(11) NOT NULL,
  `group_name` varchar(255) NOT NULL,
  `permission` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Data exporting was unselected.


-- Dumping structure for table db_penjualan.header
CREATE TABLE IF NOT EXISTS `header` (
  `id` int(10) NOT NULL auto_increment,
  `amount` int(10) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_penjualan.m_dept
CREATE TABLE IF NOT EXISTS `m_dept` (
  `code_dept` char(5) NOT NULL,
  `name_dept` varchar(50) default NULL,
  `create_user` char(8) default NULL,
  `create_time` char(20) default NULL,
  `update_time` char(20) default NULL,
  `update_user` char(20) default NULL,
  PRIMARY KEY  (`code_dept`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_penjualan.supplier
CREATE TABLE IF NOT EXISTS `supplier` (
  `id_spl` int(11) NOT NULL auto_increment,
  `kode_supplier` char(6) NOT NULL default '',
  `nama_supplier` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `telephone` varchar(100) NOT NULL,
  `fax` varchar(50) NOT NULL,
  `no_rekening` char(20) NOT NULL,
  `bank` char(50) NOT NULL,
  `atas_nama` char(50) NOT NULL,
  `no_rekening1` char(50) NOT NULL,
  `status` char(10) NOT NULL,
  `term` int(11) NOT NULL,
  `npwp` varchar(50) NOT NULL,
  `aktif` char(6) NOT NULL,
  `ap` char(1) NOT NULL default 'Y',
  `ar` char(1) NOT NULL default 'N',
  `type` char(5) NOT NULL,
  `COMPANY` varchar(100) default NULL,
  `lastname` varchar(100) default NULL,
  `firstname` varchar(100) default NULL,
  `email` varchar(50) default NULL,
  `mobile` varchar(20) default NULL,
  `city` varchar(50) default NULL,
  `country` varchar(20) default NULL,
  `segment` varchar(50) default NULL,
  `kode_marketing` char(10) default NULL,
  `remark` varchar(50) default NULL,
  `limit_Acc` decimal(20,0) default NULL,
  `Remark_Acc` longtext,
  `terbilang_limit` varchar(250) default NULL,
  `credit_f` int(11) default NULL,
  `date` date default NULL,
  PRIMARY KEY  (`kode_supplier`),
  KEY `id_spl` (`id_spl`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_penjualan.tbl_barang
CREATE TABLE IF NOT EXISTS `tbl_barang` (
  `barang_id` varchar(15) NOT NULL,
  `barang_nama` varchar(150) default NULL,
  `barang_satuan` varchar(30) default NULL,
  `barang_harpok` double default NULL,
  `barang_harjul` double default NULL,
  `barang_harjul_grosir` double default NULL,
  `barang_stok` int(11) default '0',
  `barang_min_stok` int(11) default '0',
  `barang_tgl_input` timestamp NULL default CURRENT_TIMESTAMP,
  `barang_tgl_last_update` datetime default NULL,
  `barang_kategori_id` int(11) default NULL,
  `barang_user_id` int(11) default NULL,
  PRIMARY KEY  (`barang_id`),
  KEY `barang_user_id` (`barang_user_id`),
  KEY `barang_kategori_id` (`barang_kategori_id`),
  CONSTRAINT `tbl_barang_ibfk_1` FOREIGN KEY (`barang_user_id`) REFERENCES `tbl_user` (`user_id`) ON UPDATE CASCADE,
  CONSTRAINT `tbl_barang_ibfk_2` FOREIGN KEY (`barang_kategori_id`) REFERENCES `tbl_kategori` (`kategori_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_penjualan.tbl_beli
CREATE TABLE IF NOT EXISTS `tbl_beli` (
  `beli_nofak` varchar(15) default NULL,
  `beli_tanggal` date default NULL,
  `beli_suplier_id` int(11) default NULL,
  `beli_user_id` int(11) default NULL,
  `beli_kode` varchar(15) NOT NULL,
  PRIMARY KEY  (`beli_kode`),
  KEY `beli_user_id` (`beli_user_id`),
  KEY `beli_suplier_id` (`beli_suplier_id`),
  KEY `beli_id` (`beli_kode`),
  CONSTRAINT `tbl_beli_ibfk_1` FOREIGN KEY (`beli_user_id`) REFERENCES `tbl_user` (`user_id`) ON UPDATE CASCADE,
  CONSTRAINT `tbl_beli_ibfk_2` FOREIGN KEY (`beli_suplier_id`) REFERENCES `tbl_suplier` (`suplier_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_penjualan.tbl_detail_beli
CREATE TABLE IF NOT EXISTS `tbl_detail_beli` (
  `d_beli_id` int(11) NOT NULL auto_increment,
  `d_beli_nofak` varchar(15) default NULL,
  `d_beli_barang_id` varchar(15) default NULL,
  `d_beli_harga` double default NULL,
  `d_beli_jumlah` int(11) default NULL,
  `d_beli_total` double default NULL,
  `d_beli_kode` varchar(15) default NULL,
  PRIMARY KEY  (`d_beli_id`),
  KEY `d_beli_barang_id` (`d_beli_barang_id`),
  KEY `d_beli_nofak` (`d_beli_nofak`),
  KEY `d_beli_kode` (`d_beli_kode`),
  CONSTRAINT `tbl_detail_beli_ibfk_1` FOREIGN KEY (`d_beli_barang_id`) REFERENCES `tbl_barang` (`barang_id`) ON UPDATE CASCADE,
  CONSTRAINT `tbl_detail_beli_ibfk_2` FOREIGN KEY (`d_beli_kode`) REFERENCES `tbl_beli` (`beli_kode`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_penjualan.tbl_detail_jual
CREATE TABLE IF NOT EXISTS `tbl_detail_jual` (
  `d_jual_id` int(11) NOT NULL auto_increment,
  `d_jual_nofak` varchar(15) default NULL,
  `d_jual_barang_id` varchar(15) default NULL,
  `d_jual_barang_nama` varchar(150) default NULL,
  `d_jual_barang_satuan` varchar(30) default NULL,
  `d_jual_barang_harpok` double default NULL,
  `d_jual_barang_harjul` double default NULL,
  `d_jual_qty` int(11) default NULL,
  `d_jual_diskon` double default NULL,
  `d_jual_total` double default NULL,
  PRIMARY KEY  (`d_jual_id`),
  KEY `d_jual_barang_id` (`d_jual_barang_id`),
  KEY `d_jual_nofak` (`d_jual_nofak`),
  CONSTRAINT `tbl_detail_jual_ibfk_1` FOREIGN KEY (`d_jual_barang_id`) REFERENCES `tbl_barang` (`barang_id`) ON UPDATE CASCADE,
  CONSTRAINT `tbl_detail_jual_ibfk_2` FOREIGN KEY (`d_jual_nofak`) REFERENCES `tbl_jual` (`jual_nofak`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_penjualan.tbl_jual
CREATE TABLE IF NOT EXISTS `tbl_jual` (
  `jual_nofak` varchar(15) NOT NULL,
  `jual_tanggal` timestamp NULL default CURRENT_TIMESTAMP,
  `jual_total` double default NULL,
  `jual_jml_uang` double default NULL,
  `jual_kembalian` double default NULL,
  `jual_user_id` int(11) default NULL,
  `jual_keterangan` varchar(20) default NULL,
  PRIMARY KEY  (`jual_nofak`),
  KEY `jual_user_id` (`jual_user_id`),
  CONSTRAINT `tbl_jual_ibfk_1` FOREIGN KEY (`jual_user_id`) REFERENCES `tbl_user` (`user_id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_penjualan.tbl_kategori
CREATE TABLE IF NOT EXISTS `tbl_kategori` (
  `kategori_id` int(11) NOT NULL auto_increment,
  `kategori_nama` varchar(35) default NULL,
  PRIMARY KEY  (`kategori_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_penjualan.tbl_retur
CREATE TABLE IF NOT EXISTS `tbl_retur` (
  `retur_id` int(11) NOT NULL auto_increment,
  `retur_tanggal` timestamp NULL default CURRENT_TIMESTAMP,
  `retur_barang_id` varchar(15) default NULL,
  `retur_barang_nama` varchar(150) default NULL,
  `retur_barang_satuan` varchar(30) default NULL,
  `retur_harjul` double default NULL,
  `retur_qty` int(11) default NULL,
  `retur_subtotal` double default NULL,
  `retur_keterangan` varchar(150) default NULL,
  PRIMARY KEY  (`retur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_penjualan.tbl_suplier
CREATE TABLE IF NOT EXISTS `tbl_suplier` (
  `suplier_id` int(11) NOT NULL auto_increment,
  `suplier_nama` varchar(35) default NULL,
  `suplier_alamat` varchar(60) default NULL,
  `suplier_notelp` varchar(20) default NULL,
  PRIMARY KEY  (`suplier_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_penjualan.tbl_user
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` int(11) NOT NULL auto_increment,
  `user_nama` varchar(35) default NULL,
  `user_username` varchar(30) default NULL,
  `user_password` varchar(35) default NULL,
  `user_level` varchar(2) default NULL,
  `user_status` varchar(2) default '1',
  PRIMARY KEY  (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_penjualan.tb_kode_perkiraan
CREATE TABLE IF NOT EXISTS `tb_kode_perkiraan` (
  `kode_perkiraan` char(15) NOT NULL,
  `id` int(11) NOT NULL auto_increment,
  `coa_property` char(15) NOT NULL,
  `nama_perkiraan` varchar(100) default NULL,
  `kelompok` varchar(10) default NULL,
  `nama_kelompok` varchar(50) default NULL,
  `hheader` varchar(50) default NULL,
  `jenis` char(1) default NULL,
  `rel` char(1) default NULL,
  `cust` char(1) default NULL,
  `cash_bank` char(1) default NULL,
  `neraca` char(1) default NULL,
  `aktif` char(1) default NULL,
  `drr_setup` char(20) default NULL,
  `drr_setup_header` char(20) default NULL,
  `drr_setup_header_no` int(11) default NULL,
  `piutang_hutang` char(50) default NULL,
  `pl_fo` int(11) default NULL,
  `pl_eng` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.


-- Dumping structure for table db_penjualan.tmp_kartu_stok
CREATE TABLE IF NOT EXISTS `tmp_kartu_stok` (
  `no_record` int(11) NOT NULL auto_increment,
  `ccode` char(50) default NULL,
  `nama_barang` varchar(50) default NULL,
  `no_Tran` varchar(50) default NULL,
  `date` varchar(50) default NULL,
  `symbol` varchar(50) default NULL,
  `fbegin` int(11) default NULL,
  `fin` int(11) default NULL,
  `fout` int(11) default NULL,
  `r_in` int(11) default NULL,
  `r_sr` int(11) default NULL,
  `ajust` int(11) default NULL,
  `balance` int(11) default NULL,
  PRIMARY KEY  (`no_record`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
